import java.io.*;
import java.net.*; // import des bibliothèques pour le réseau

public class ClientUDP {

    final static int port = 8080; // on détermine le port 8080
    final static int taille = 1024; // c'est la taille max d'un message UDP
    static byte buffer[] = new byte[taille]; // on ne va pas l'utiliser ici mais sinon c'est pour créer un tableau de type byte pour trouver la tailler du buffer

    public static void main(String argv[]) throws Exception {
        try {
            InetAddress server = InetAddress.getLocalHost(); // met dans la variable server les infos de l'adresse IP et du nom de l'ordi avec getlocalhost
            System.out.println("Inet : " + server); // on affiche les données du server
            int length = 1024; // on instancie la variable length à 1024
            // byte buffer[] = argv[1].getBytes(); // On enlève car ça faisait un bug
            //System.out.println("buffer" + buffer);

            DatagramSocket socket = new DatagramSocket(); // On preépare la socket
            DatagramPacket donneesEmises = new DatagramPacket(buffer, length, server, port); // on crée un paquet a emmettre dans notre socket
            DatagramPacket donneesRecues = new DatagramPacket(new byte[taille], taille); // On créé un paquet à recevoir toujours via la socket

            socket.setSoTimeout(30000); // on met le time out à 30 sec
            socket.send(donneesEmises); // on envoie notre paquet créé plus haut
            socket.receive(donneesRecues); // on écoute la socket pour savoir si on recoit des infos

            System.out.println("Message : " + new String(donneesRecues.getData(), 0, donneesRecues.getLength())); // on affiche les données recues et on récupère la taille de la donnée
            System.out.println("de : " + donneesRecues.getAddress() + ":" + donneesRecues.getPort()); // On affiche l'expediteur avec getAddress
        }catch (SocketTimeoutException ste) {
            System.out.println("Le delai pour la reponse a expire"); // on gère l'exception liée au timeout
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
