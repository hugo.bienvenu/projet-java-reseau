import java.net.*;
import java.io.*;

public class ServerTCP {
    final static int port = 8080;

    public static void main(String[] args) {
        try {
            ServerSocket socketServeur = new ServerSocket(port);
            System.out.println("Lancement du serveur "+socketServeur);

            while (true) {
                Socket socketClient = socketServeur.accept();
                String message = "WEEEESH";

                System.out.println("Connexion avec : "+socketClient.getInetAddress());

                // InputStream in = socketClient.getInputStream();
                // OutputStream out = socketClient.getOutputStream();

                BufferedReader in = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
                PrintStream out = new PrintStream(socketClient.getOutputStream());
                message = in.readLine();
                out.println(message);

                socketClient.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
