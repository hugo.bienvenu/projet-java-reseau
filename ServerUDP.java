import java.io.*;
import java.net.*;

public class ServerUDP {

    final static int port = 8080; // on initie le port 8080
    final static int taille = 1024; // on initie la taille à 1024
    static byte buffer[] = new byte[taille]; // on crée un tableau buffer de type byte

    public static void main(String argv[]) throws Exception {
        DatagramSocket socket = new DatagramSocket(port); // on crée une socket UDP avec Datagram
        String donnees = ""; // on inite les données à 0
        String message = ""; // on initie les données à 0
        int taille = 0; // on initie les données à 0

        System.out.println("Lancement du serveur"); // On affiche qu'on va lancer le serveur
        while (true) {
            DatagramPacket paquet = new DatagramPacket(buffer, buffer.length); //on crée une variable paquet à envoyer ou recevoir de type Datagram
            DatagramPacket envoi = null; // on initie la variable envoi à 0
            socket.receive(paquet); // On écoute la socket pour savoir si on recoit un paquet ( on le recoit sous la forme de la variable paquet qui est un Datagram

            System.out.println("\n"+paquet.getAddress()); // on affiche l'addresse du paquet qu'on a reçu
            taille = paquet.getLength(); // on récupère la taille de ce paquet
            donnees = new String(paquet.getData(),0, taille); // on récupère les données de ce paquet
            System.out.println("Donnees reçues = "+donnees); // on affiche les données reçues

            message = "Wsh bien ? Tu dates !"+donnees; // Voici le message à envoyer à une personne qu'on a pas vu depuis longtemps
            System.out.println("Donnees envoyees = "+message); // On affiche le message qu'on envoie
            envoi = new DatagramPacket(message.getBytes(), message.length(), paquet.getAddress(), paquet.getPort()); // on crée une variable envoi de type Datagram qui va contenir les infos nécessaires à l'envoi d'un Datagram via socket
            socket.send(envoi); // on envoie notre message via la socket
        }
    }
}
